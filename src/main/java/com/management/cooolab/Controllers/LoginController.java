package com.management.cooolab.Controllers;

import com.management.cooolab.Entities.User;
import com.management.cooolab.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {
    @Autowired
    UserService userService;
    @GetMapping("/login")
    public String login() {
        return "login";
    }
    @PostMapping("/login")
    public String loginUser(@RequestParam String email, @RequestParam String password, Model model) {
        User user = userService.getUsersByEmailAndPassword(email, password);
        if (user != null) {
            model.addAttribute("user", user);
            return "redirect:/leaveRequest/mine/list";
        } else {
            model.addAttribute("error", "Email ou mot de passe incorrect.");
            return "login";
        }
    }
}
