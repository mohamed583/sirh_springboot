package com.management.cooolab.Controllers;

import com.management.cooolab.Entities.DemandeConge;
import com.management.cooolab.Entities.Departement;
import com.management.cooolab.Entities.User;
import com.management.cooolab.Services.DemandeCongeService;
import com.management.cooolab.Services.DepartmentService;
import com.management.cooolab.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private DemandeCongeService demandeCongeService;
    @GetMapping("/list")
    public String ListUsers(Model model){
        List<User> users = userService.getAllUsers();
        model.addAttribute("users", users);
        return "User/ShowAll";
    }
    @GetMapping("/add")
    public String addUser(Model model) {
        User user = new User();
        List<Departement> departments = departmentService.getAllDepartments();
        List<String> privileges = Arrays.asList("admin", "hr", "manager", "employee");
        model.addAttribute("departments", departments);
        model.addAttribute("privileges", privileges);
        model.addAttribute("user", user);
        return "User/Add";
    }
    @PostMapping("/save")
    public String save(@ModelAttribute("user") User user, @RequestParam("privilege") String privilege) {
        switch (privilege) {
            case "admin":
                user.setHr(true);
                user.setManager(true);
                break;
            case "hr":
                user.setHr(true);
                user.setManager(false);
                break;
            case "manager":
                user.setHr(false);
                user.setManager(true);
                break;
            case "employee":
            default:
                user.setHr(false);
                user.setManager(false);
                break;
        }
        userService.registerUser(user);
        return "redirect:/user/list";
    }

    @GetMapping("/edit/{id}")
    public String EditUser(@PathVariable("id") int id, Model model) {
        User user = userService.getUserById(id);
        List<Departement> departments = departmentService.getAllDepartments();
        model.addAttribute("departments", departments);
        model.addAttribute("user", user);
        return "User/Edit";
    }

    @PostMapping("/update")
    public String UpdateUser(@ModelAttribute("user") User user, @RequestParam("privilege") String privilege) {
        switch (privilege) {
            case "admin":
                user.setHr(true);
                user.setManager(true);
                break;
            case "hr":
                user.setHr(true);
                user.setManager(false);
                break;
            case "manager":
                user.setHr(false);
                user.setManager(true);
                break;
            case "employee":
                user.setHr(false);
                user.setManager(false);
                break;
        }

        userService.saveUser(user);
        return "redirect:/user/list";
    }
    @GetMapping("/delete/{id}")
    public String DeleteUser(@PathVariable("id") int id){
        userService.deleteUser(id);
        return "redirect:/user/list";
    }
    @GetMapping("/show/{id}")
    public String showUser(@PathVariable("id") int userId, Model model) {
        User user = userService.getUserById(userId);
        List<DemandeConge> demandesConges = demandeCongeService.getDemandeCongeByEmployeeId(userId);
        if (demandesConges == null) {
            return "redirect:/user/list";
        }
        model.addAttribute("demandesConges",demandesConges);
        model.addAttribute("user", user);
        return "User/Show";
    }
}
