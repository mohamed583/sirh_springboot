package com.management.cooolab.Controllers;

import com.management.cooolab.Entities.Departement;
import com.management.cooolab.Entities.User;
import com.management.cooolab.Services.DepartmentService;
import com.management.cooolab.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/department")
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private UserService userService;
    @GetMapping("/list")
    public String ListDepartments(Model model){
        List<Departement> departments = departmentService.getAllDepartments();
        model.addAttribute("departments", departments);
        return "Department/ShowAll";
    }
    @GetMapping("/add")
    public String AddDepartment(Model model){
        Departement departement = new Departement();
        model.addAttribute("departement", departement);
        return "Department/Add";
    }
    @PostMapping("/save")
    public  String save (@ModelAttribute("departement") Departement departement){
        departmentService.saveDepartment(departement);
        return "redirect:/department/list";
    }
    @GetMapping("/edit/{id}")
    public String EditDepartment(@PathVariable("id") int id, Model model){
        Departement department = departmentService.getDepartmentById(id);
        departmentService.saveDepartment(department);
        model.addAttribute("departement", department);
        return "Department/Edit";
    }
    @PostMapping("/update")
    public String UpdateDepartment(@Validated Departement department, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return "redirect:/department/edit/"+ department.getId();
        }
        departmentService.saveDepartment(department);
        return "redirect:/department/list";
    }
    @GetMapping("/delete/{id}")
    public String deleteDepartment(@PathVariable("id") int id, @RequestParam(value = "deleteUsers", defaultValue = "false") boolean deleteUsers) {
        Departement department = departmentService.getDepartmentById(id);

        if (department != null) {
            List<User> users = userService.getUsersByDepartment(id);

            if (deleteUsers) {
                for (User user : users) {
                    userService.deleteUser(user.getId());
                }
            } else {
                for (User user : users) {
                    user.setDepartement(null);
                    userService.saveUser(user);
                }
            }

            departmentService.deleteDepartment(id);
        }

        return "redirect:/department/list";
    }
    @GetMapping("/show/{id}")
    public String showDepartment(@PathVariable("id") int id, Model model) {
        Departement department = departmentService.getDepartmentById(id);
        List<User> users = userService.getUsersByDepartment(id);
        if (department == null) {
            return "redirect:/department/list";
        }
        model.addAttribute("users",users);
        model.addAttribute("department", department);
        return "Department/Show";
    }


}
