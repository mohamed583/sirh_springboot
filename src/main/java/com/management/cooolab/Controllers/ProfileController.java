package com.management.cooolab.Controllers;

import com.management.cooolab.Entities.User;
import com.management.cooolab.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/profile")
public class ProfileController {

    @Autowired
    private UserService userService;

    @GetMapping("")
    public String editProfile(Model model) {
        User user = userService.getCurrentUser();
        model.addAttribute("user", user);
        return "Profile";
    }

    @PostMapping("/update")
    public String updateProfile(@Validated @ModelAttribute("user") User updatedUser, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "Profile/Edit";
        }

        userService.saveUser(updatedUser);
        return "redirect:/login";
    }
}